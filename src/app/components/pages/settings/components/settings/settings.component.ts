import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ItemEventData, Page } from '@nativescript/core';
import { HelpAppInfoComponent } from '../app-info/app-info.component';

@Component({
    selector: 'app-settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.scss']
})

export class SettingsComponent implements OnInit {

    settingsOptions: Array<any> = [];

    constructor(
        private page: Page,
        private routerExtensions: RouterExtensions
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.settingsOptions = [
            /* {
                id: 0,
                icon: String.fromCharCode(0xf502),
                title: 'Privacidad',
                subtitle: 'Cambio de contraseña',
                url: '/settings/privacity'
            }, */
            {
                id: 1,
                icon: String.fromCharCode(0xf502),
                title: 'Seguridad',
                subtitle: 'Cambio de contraseña',
                url: '/settings/security'
            },
            {
                id: 2,
                icon: String.fromCharCode(0xf059),
                title: 'Ayuda',
                subtitle: 'Aviso de privacidad, info. de la aplicación',
                url: '/settings/help'
            }
        ]
    }

    onItemTap(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: 'slide'
            }
        })
    }
}