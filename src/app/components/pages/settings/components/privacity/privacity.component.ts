import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS, Page } from '@nativescript/core';

import * as fileSystem from '@nativescript/core/file-system';

@Component({
    selector: 'app-settings-privacity',
    templateUrl: 'privacity.component.html',
    styleUrls: ['privacity.component.scss']
})

export class HelpPrivacityComponent implements OnInit {

    titleActionBar: string = 'Aviso de Privacidad';
    isAndroid: boolean;
    isIOS: boolean;

    PDF_URL: string = '';
    documentName: string = 'aviso-privacidad.pdf';

    constructor(
        private routerExtensions: RouterExtensions, private page: Page
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
        this.page.actionBarHidden = true;
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    ngOnInit() {
        this.viewPDF();
    }

    private viewPDF() {
        const currentAppFolder = fileSystem.knownFolders.currentApp();
        const filePath = fileSystem.path.join(currentAppFolder.path, 'assets', 'docs', 'aviso-privacidad', this.documentName);
        this.PDF_URL = filePath;
    }

    onLoad() {
        console.log('PDF cargado correctamente');
    }
}