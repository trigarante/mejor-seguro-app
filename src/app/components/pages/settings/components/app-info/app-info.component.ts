import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS, Page } from '@nativescript/core';

import * as appVersion from '@nativescript/appversion';

@Component({
    selector: 'app-app-info',
    templateUrl: 'app-info.component.html',
    styleUrls: ['app-info.component.scss']
})

export class HelpAppInfoComponent implements OnInit {

    titleActionBar: string = 'Info. de la Aplicación ';

    isAndroid: boolean;
    isIOS: boolean;

    versionApp: string = '';

    constructor(
        private routerExtension: RouterExtensions, private page: Page
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
        this.page.actionBarHidden = true;
    }

    goBack() {
        this.routerExtension.backToPreviousPage();
    }

    ngOnInit() {
        let version = '';
        appVersion.getVersionName()
            .then(function (v) {
                console.log("Your app's version is: " + v);
                version = v;
            })
            .finally(() => {
                this.versionApp = version;
            });

        appVersion.getAppId().then((id: string) => {
            console.log("Your app's id is: " + id);
        });
    }
}