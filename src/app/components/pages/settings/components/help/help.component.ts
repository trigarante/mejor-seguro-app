import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS, Page } from '@nativescript/core';

@Component({
    selector: 'app-settings-privacity',
    templateUrl: 'help.component.html',
    styleUrls: ['help.component.scss']
})

export class SettingsHelpComponent implements OnInit {

    titleActionBar: string = 'Seguridad';
    helpOptions: Array<any> = [];

    isIOS: any;
    isAndroid: any;

    constructor(
        private page: Page,
        private routerExtensions: RouterExtensions
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() {
        this.helpOptions = [
            {
                id: 0,
                icon: String.fromCharCode(0xf023),
                title: 'Aviso de privacidad',
                url: 'settings/help/privacity'
            },
            {
                id: 1,
                icon: String.fromCharCode(0xf05a),
                title: 'Info. de la aplicación',
                url: 'settings/help/info'
            }
        ]
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onItemTap(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: 'slide'
            }
        })
    }
}