import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptCommonModule, NativeScriptFormsModule } from '@nativescript/angular';
import { SecurityPasswordComponent } from './components/password/password.component';
import { SettingsHelpComponent } from './components/help/help.component';
import { SettingsSecurityComponent } from './components/security/security.component';
import {HelpPrivacityComponent} from './components/privacity/privacity.component';
import { HelpAppInfoComponent } from './components/app-info/app-info.component';


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        SettingsHelpComponent,
        SettingsSecurityComponent,
        SecurityPasswordComponent,
        HelpPrivacityComponent,
        HelpAppInfoComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SettingsModule { }
