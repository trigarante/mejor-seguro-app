import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { PetitionClass } from '~/app/core/Classes/Policies/petitions/Petition@Class';

import * as TNSPhone from 'nativescript-phone';
import { isAndroid, isIOS, Page, Utils } from '@nativescript/core';

@Component({
    selector: 'app-payment-instructions-component',
    templateUrl: 'payment-instructions.component.html',
    styleUrls: ['payment-instructions.component.scss']
})

export class PaymentInstructions implements OnInit {

    petitionsArr: Array<any>;
    isAccordionOpen: boolean = false;
    idToOpen: number = -1;

    insurersGroupCollection: Array<{ id: string, nota1: string, nota2: string, urlIMG: string, documento: string, active: boolean }>;

    petitionClass: PetitionClass = new PetitionClass();
    images: Array<any>;

    isAndroid: any;
    isIOS: any;

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page
    ) {
        this.page.actionBarHidden = true;
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() {
        this.petitionsArr = this.petitionClass.petitionsArr;
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    call(phone: string) {
        if (isAndroid) {
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(phone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(phone, false);
                });
        }
        else if (isIOS) {
            TNSPhone.dial(phone, true);
        }
    }

    openEmail(email: string) {
        const mailto: string = `mailto:${email}`;
        Utils.openUrl(mailto);
    }

    onTapButtonNavigation(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: 'fade'
            }
        })
    }

    goToInspection() {
        this.routerExtensions.navigate(['vehicle-inspection'], {
            transition: {
                name: 'slide'
            }
        })
    }
}