import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { ConcentratorAccountsComponent } from './components/concentrator-accounts/concentrator-accounts.component';
import { ViewDocumentComponent } from './components/view-document/view-document.component';


@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    declarations: [
        ConcentratorAccountsComponent,
        ViewDocumentComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PaymentInstructionsModule { }