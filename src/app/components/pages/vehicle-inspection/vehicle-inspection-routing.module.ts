import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { VehicleInspectionComponent } from './components/vehicle-inspection.component';

const routes: Routes = [
    {
        path: '',
        component: VehicleInspectionComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class VehicleInspectionRoutingModule { }
