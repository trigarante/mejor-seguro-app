import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { VehicleInspectionComponent } from './components/vehicle-inspection.component';
import { VehicleInspectionRoutingModule } from './vehicle-inspection-routing.module';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        VehicleInspectionRoutingModule
    ],
    exports: [],
    declarations: [
        VehicleInspectionComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class VehicleInspectionModule { }
