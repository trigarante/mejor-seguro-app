import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { LogoutComponent } from '../logout/components/logout.component';
import { EventOfClaimComponent } from './event-of-claim/components/event-of-claim.component';
import { HomeComponent } from './home/components/home/home.component';
import { OfficialIdentificationComponent } from './official-identification/components/official-identification.component';
import { PetitionsComponent } from './petitions/components/petitions.component';
import { PoliciesComponent } from './policies/components/policies/policies.component';
import { ProfileComponent } from './profile/components/profile.component';
import { SettingsComponent } from './settings/components/settings/settings.component';
import { SupportComponent } from './support/components/support/support.component';
import { VehicleInspectionComponent } from './vehicle-inspection/components/vehicle-inspection.component';
import { PaymentInstructions } from './payment-instructions/components/payment-instructions/payment-instructions.component';
import { BillingCatalog } from './billing-catalog/billing-catalog.component';
import { GeneralConditionsComponent } from './support/components/general-conditions/general-conditions.component';
import { PrivacityComponent } from '~/app/shared/components/privacity/privacity.component';
import { HelpPrivacityComponent } from './settings/components/privacity/privacity.component';
import { HelpAppInfoComponent } from './settings/components/app-info/app-info.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'policies',
        component: PoliciesComponent
    },
    {
        path: 'petitions',
        component: PetitionsComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'support',
        component: SupportComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    },
    {
        path: 'event-of-claim',
        component: EventOfClaimComponent
    },
    {
        path: 'official-identification',
        component: OfficialIdentificationComponent
    },
    {
        path: 'vehicle-inspection',
        component: VehicleInspectionComponent
    },
    {
        path: 'payment-instructions',
        component:  PaymentInstructions
    },
    {
        path: 'billing-catalog',
        component: BillingCatalog
    },
    {
        path: 'general-conditions',
        component: GeneralConditionsComponent
    },
    {
        path: 'privacy-notice',
        component: HelpPrivacityComponent
    },
    {
        path: 'app-info',
        component: HelpAppInfoComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PagesRoutingModule { }