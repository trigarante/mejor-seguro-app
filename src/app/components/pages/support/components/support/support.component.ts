import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { PetitionClass } from '~/app/core/Classes/Policies/petitions/Petition@Class';

import * as TNSPhone from 'nativescript-phone';
import { isAndroid, isIOS, Page, Utils } from '@nativescript/core';

@Component({
    selector: 'app-support-component',
    templateUrl: 'support.component.html',
    styleUrls: ['support.component.scss']
})

export class SupportComponent implements OnInit {

    petitionsArr: Array<any>;
    isAccordionOpen: boolean = false;
    idToOpen: number = -1;

    insurersGroupCollection: Array<{ id: string, nota1: string, nota2: string, urlIMG: string, documento: string, active: boolean }>;

    petitionClass: PetitionClass = new PetitionClass();
    images: Array<any>;

    isAndroid: any;
    isIOS: any;

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page
    ) {
        this.page.actionBarHidden = true;
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() {
        this.petitionsArr = this.petitionClass.petitionsArr;
        this.selectImage();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    actionAccordion(id: number) {
        if (this.idToOpen === id) {
            if (this.isAccordionOpen) {
                this.isAccordionOpen = false;
            } else {
                this.isAccordionOpen = true;
            }
        } else {
            this.idToOpen = id;
            this.isAccordionOpen = true;
        }
    }



    call(phone: string) {
        if (isAndroid) {
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(phone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(phone, false);
                });
        }
        else if (isIOS) {
            TNSPhone.dial(phone, true);
        }
    }

    selectImage() {
        const url: string = `~/assets/iconos-aseguradoras`;
        this.images = [
            { alias: "ABA", name: "aba.svg", tel: '800 712 2828', phone: '8007122828' },
            { alias: "SEGUROS AFIRME", name: "afirme.svg", tel: '800 723 4763', phone: '8007234763' },
            { alias: "ANA SEGUROS", name: "ana.svg", tel: '800 911 2627 ', phone: '8009112627' },
            { alias: "AXA", name: "axa.svg", tel: '800 900 1292', phone: '8009001292' },
            { alias: "BANORTE", name: "banorte.svg", tel: '800 500 1500', phone: '8005001500' },
            { alias: "GENERAL DE SEGUROS", name: "general.svg", tel: '800 472 7696', phone: '8004727696' },
            { alias: "GNP", name: "gnp.svg", tel: '800 400 9000', phone: '8004009000' },
            { alias: "HDI", name: "hdi.svg", tel: '800 019 6000', phone: '8000196000' },
            { alias: "INBURSA", name: "inbursa.svg", tel: '800 909 0000', phone: '8009090000' },
            { alias: "MAPFRE", name: "mapfre.svg", tel: '55 5950 7777 ', phone: '5559507777 ' },
            { alias: "MIGO", name: "migo.svg", tel: '800 00 90 000', phone: '8000090000' },
            { alias: "EL POTOSI", name: "elpotosi.svg", tel: '800 00 90 000', phone: '8000090000' },
            { alias: "QUALITAS", name: "qualitas.svg", tel: '800 800 2880', phone: '8008002880' },
            { alias: "ZURA", name: "sura.svg", tel: '800 911 7692', phone: '8009117692' },
            { alias: "ZURICH", name: "zurich.svg", tel: '800 288 6911', phone: '8002886911' },
            { alias: "EL AGUILA", name: "elaguila.svg", tel: '800 705 8800', phone: '8007058800' },
            { alias: "AIG", name: "aig.svg", tel: '800 001 1300', phone: '8000011300' },
            { alias: "LA LATINO", name: "lalatino.svg", tel: '800 685 1170', phone: '8006851170' }
        ];

        /* return `${url}/${images.find((e) => e.alias === alias).name}`; */
    }

    selectSize(alias: string, typeSize: string) {
        const sizes = [
            {
                alias: "ABA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "SEGUROS AFIRME",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "ANA SEGUROS",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "AXA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "BANORTE",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "GENERAL DE SEGUROS",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "GNP",
                heightImg: '200px',
                widthImg: '450px',
            },
            {
                alias: "HDI",
                heightImg: '200px',
                widthImg: '450px',
            },
            {
                alias: "INBURSA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "MAPFRE",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "MIGO",
                heightImg: '150px',
                widthImg: '350px'
            },
            {
                alias: "EL POTOSI",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "QUALITAS",
                heightImg: '180px',
                widthImg: '450px'
            },
            {
                alias: "ZURA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "ZURICH",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "EL AGUILA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "AIG",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "LA LATINO",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "Atlas Seguros",
                heightImg: '300px',
                widthImg: '300px'
            },
            {
                alias: "Bx+",
                heightImg: '200px',
                widthImg: '300px'
            }
        ];

        if (typeSize === 'w') {
            return sizes.find((a) => a.alias === alias).widthImg
        } else if (typeSize === 'h') {
            return sizes.find((a) => a.alias === alias).heightImg
        }
    }

    openEmail(email: string) {
        const mailto: string = `mailto:${email}`;
        Utils.openUrl(mailto);
    }

    onTapButtonNavigarion(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: 'fade'
            }
        })
    }

    goToInspection() {
        this.routerExtensions.navigate(['vehicle-inspection'], {
            transition: {
                name: 'slide'
            }
        })
    }
}