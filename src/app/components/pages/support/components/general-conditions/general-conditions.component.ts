import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS, Page } from '@nativescript/core';

@Component({
    selector: 'app-general-conditions',
    templateUrl: 'general-conditions.component.html',
    styleUrls: ['general-conditions.component.scss']
})

export class GeneralConditionsComponent implements OnInit {

    titleActionBar: string = 'Condiciones Generales por Aseguradora';

    isAndroid: boolean;
    isIOS: boolean;

    insurersGroupCollection: any;

    constructor(
        private routerExtensions: RouterExtensions, private page: Page
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
        this.page.actionBarHidden = true;
        this.insureCollection();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    ngOnInit() { }

    insureCollection() {
        this.insurersGroupCollection = [
            {
                id: 'ABA',
                imageName: 'aba.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'aba-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'SEGUROS AFIRME',
                imageName: 'afirme.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'afirme-condiciones-generales.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'Taxi Afirme',
                        generalConditionsOtherDoc: 'afirme-condiciones-generales-taxi.pdf'
                    }
                ]
            },
            {
                id: 'ANA SEGUROS',
                imageName: 'ana.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'ana-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'AXA',
                imageName: 'axa.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'axa-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'BANORTE',
                imageName: 'banorte.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'banorte-condiciones-generales.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'Seguro de Taxi',
                        generalConditionsOtherDoc: 'banorte-condiciones-generales-taxi.pdf'
                    }
                ]
            },
            {
                id: 'GENERAL DE SEGUROS',
                imageName: 'general.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'general-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'GNP',
                imageName: 'gnp.svg',
                conditionsGeneralsTitle: 'Auto Individual',
                conditionsGeneralsDoc: 'gnp-condiciones-generales-auto-individual.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'Chofer Privado',
                        generalConditionsOtherDoc: 'gnp-condiciones-generales-chofer-privado.pdf'
                    }
                ]
            },
            {
                id: 'HDI',
                imageName: 'hdi.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'hdi-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'INBURSA',
                imageName: 'inbursa.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: '',
                otherDocuments: false
            },
            {
                id: 'MAPFRE',
                imageName: 'mapfre.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'mapfre-condiciones-generales.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'Tú Eliges',
                        generalConditionsOtherDoc: 'mapfre-condiciones-generales-tu-eliges.pdf'
                    }
                ]
            },
            {
                id: 'MIGO',
                imageName: 'migo.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
            },
            {
                id: 'PRIMERO SEGUROS',
                imageName: 'primero.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'primero-seguros-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'EL POTOSÍ',
                imageName: 'el-potosi.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'el-potosi-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'QUALITAS',
                imageName: 'qualitas.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'qualitas-condiciones-generales.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'De Carga',
                        generalConditionsOtherDoc: 'qualitas-condiciones-generales-carga.pdf'
                    },
                    {
                        generalConditionsOtherTitle: 'Motos',
                        generalConditionsOtherDoc: 'qualitas-condiciones-generales-motos.pdf'
                    },
                    {
                        generalConditionsOtherTitle: 'Servicio Público',
                        generalConditionsOtherDoc: 'qualitas-condiciones-generales-servicio-publico.pdf'
                    }
                ]
            },
            {
                id: 'SURA',
                imageName: 'sura.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'sura-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'ZURICH',
                imageName: 'zurich.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'zurich-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'EL ÁGUILA',
                imageName: 'el-aguila.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'el-aguila-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'AIG',
                imageName: 'aig.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'aig-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'LA LATINO',
                imageName: 'la-latino.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'la-latino-condiciones-generales.pdf',
                otherDocuments: true,
                otherDocumentsName: [
                    {
                        generalConditionsOtherTitle: 'Taxi Latino',
                        generalConditionsOtherDoc: 'la-latino-condiciones-generales-taxi.pdf'
                    }
                ]
            },
            {
                id: 'ATLAS SEGUROS',
                imageName: 'atlas.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'atlas-condiciones-generales.pdf',
                otherDocuments: false
            },
            {
                id: 'Bx+',
                imageName: 'bxmas.svg',
                conditionsGeneralsTitle: 'Condiciones Generales',
                conditionsGeneralsDoc: 'bx+-condiciones-generales.pdf',
                otherDocuments: false
            },
        ];
    }

    openGeneralConditionsPDF(documentName: string) {
        const arrayData = JSON.stringify({ document: documentName, folder: 'general-conditions', title: 'Condiciones Generales' });
        this.routerExtensions.navigate(['/support/concentrator-account/view-document', arrayData], {
            transition: {
                name: 'fade'
            }
        })
    }

}