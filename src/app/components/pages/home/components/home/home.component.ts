import { Component, OnInit } from '@angular/core';
import { ApplicationSettings, Page } from '@nativescript/core';

import * as fileSystem from '@nativescript/core/file-system';
import { ClientModel } from '~/app/core/Models/Client/Client@Model';
import { DataClientService } from '~/app/core/Services/DataClient/dataclient.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
    private userActive: number;
    dataClientArr: ClientModel

    constructor(
        private page: Page,
        private dataClientservice: DataClientService
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.page.actionBarHidden = true;
        this.getDataClient();
    }

    ngOnInit() {
    }


    getDataClient() {
        let arrayDataClient: ClientModel;
        this.dataClientservice.getDataClient(this.userActive)
            .subscribe((data: ClientModel) => {
                arrayDataClient = data;
            }, error => {
                console.error('Error Load User Data Client => ', error.message);
            }, () => {
                this.dataClientArr = arrayDataClient;
            });
    }
}