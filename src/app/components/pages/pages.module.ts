import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { SharedModule } from '~/app/shared/shared.module';
import { HomeComponent } from './home/components/home/home.component';
import { OfficialIdentificationComponent } from './official-identification/components/official-identification.component';
import { PagesRoutingModule } from './pages-router.module';
import { SettingsComponent } from './settings/components/settings/settings.component';
import { SupportComponent } from './support/components/support/support.component';
import { PaymentInstructions } from './payment-instructions/components/payment-instructions/payment-instructions.component';
import { BillingCatalog } from './billing-catalog/billing-catalog.component';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PagesRoutingModule,
        SharedModule
    ],
    declarations: [
        SupportComponent,
        HomeComponent,
        SettingsComponent,
        PaymentInstructions,
        BillingCatalog
    ],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
