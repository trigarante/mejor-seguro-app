import { Component } from '@angular/core';
import { isAndroid, isIOS, Page, Utils } from '@nativescript/core';

@Component({
    selector: 'app-billing-catalog-component',
    templateUrl: 'billing-catalog.component.html',
    styleUrls: ['billing-catalog.component.scss']
})

export class BillingCatalog {

    isAndroid: any;
    isIOS: any;

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    openEmail(email: string) {
        const mailto: string = `mailto:${email}`;
        Utils.openUrl(mailto);
    }
}