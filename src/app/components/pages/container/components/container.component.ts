    import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, isAndroid, isIOS } from '@nativescript/core';
import { DrawerTransitionBase, SlideInOnTopTransition } from 'nativescript-ui-sidedrawer';
import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular';
import { filter } from 'rxjs/operators';
import { ClientModel } from '~/app/core/Models/Client/Client@Model';
import { DataClientService } from '~/app/core/Services/DataClient/dataclient.service';

@Component({
    selector: 'app-container',
    templateUrl: 'container.component.html',
    styleUrls: ['container.component.scss']
})

export class ContainerComponent implements OnInit {
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;

    private userActive: number;
    dataClientArr: ClientModel;
    nombre: string;
    lastname: string;
    wordsname: string[];
    mail: string;

    isAndroid: any;
    isIOS: any;

    /* Se almacena el estado del sidedraer para cuando se vuelva
    a presionar el mismo botón se cierre o se abra dependiendo del estado. */
    isOpenSidedrawer = false;

    @ViewChild(RadSideDrawerComponent, { static: false })
    drawerComponent: RadSideDrawerComponent;

    constructor(
        private router: Router,
        private routerExtensions: RouterExtensions,
        private dataClientservice: DataClientService
    ) {
        /* this.page.actionBarHidden = true; */
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit(): void {
        this._activatedUrl = '/container';
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            // tslint:disable-next-line: deprecation
            .subscribe((event: NavigationEnd) => (this._activatedUrl = event.urlAfterRedirects));
        this.getDataClient();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: 'fade'
            },
            clearHistory: true
        });
        /* const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer(); */
        this.drawerComponent.sideDrawer.closeDrawer();
        this.isOpenSidedrawer = false;
    }

    showDrawer() {
        if (this.isOpenSidedrawer === false) {
            this.isOpenSidedrawer = true;
            this.drawerComponent.sideDrawer.showDrawer();
        } else if (this.isOpenSidedrawer) {
            this.drawerComponent.sideDrawer.closeDrawer();
            this.isOpenSidedrawer = false;
        }
    }

    getDataClient() {
        let arrayDataClient: ClientModel;
        this.dataClientservice.getDataClient(this.userActive)
            .subscribe((data: ClientModel) => {
                arrayDataClient = data;
            }, error => {
                console.error('Error Load User Data Client => ', error.message);
            }, () => {
                this.dataClientArr = arrayDataClient;
                this.nombre = this.dataClientArr.nombre.toLowerCase();
                this.wordsname = this.nombre.split(' ');
                this.nombre = this.wordsname[0][0].toUpperCase() + this.wordsname[0].slice(1) + ' ' + this.wordsname[1][0].toUpperCase() + this.wordsname[1].slice(1);
                this.wordsname[0] = this.dataClientArr.paterno[0].toUpperCase() + this.dataClientArr.paterno.toLowerCase().slice(1);
                this.wordsname[1] = this.dataClientArr.materno[0].toUpperCase() + this.dataClientArr.materno.toLowerCase().slice(1);
                this.lastname = this.wordsname[0] + ' ' + this.wordsname[1];
                this.mail = this.dataClientArr.correo.toLowerCase();
            
            });
    }

}