import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { PoliciesComponent } from './components/policies/policies.component';

const routes: Routes = [
    {
        path: '',
        component: PoliciesComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PoliciesRoutingModule { }
