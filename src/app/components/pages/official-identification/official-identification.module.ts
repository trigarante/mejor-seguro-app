import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { OfficialIdentificationComponent } from './components/official-identification.component';
import { OfficialIdentificationRoutingModule } from './official-identification-routing.module';


@NgModule({
    imports: [
        NativeScriptCommonModule,
        OfficialIdentificationRoutingModule
    ],
    exports: [],
    declarations: [OfficialIdentificationComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class NameModule { }
