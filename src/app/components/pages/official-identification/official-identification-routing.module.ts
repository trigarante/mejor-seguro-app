import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { OfficialIdentificationComponent } from './components/official-identification.component';

const routes: Routes = [
    {
        path: '',
        component: OfficialIdentificationComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class OfficialIdentificationRoutingModule { }
