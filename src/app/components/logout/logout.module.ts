import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { SharedModule } from '~/app/shared/shared.module';
import { LogoutComponent } from './components/logout.component';
import { LogoutRoutingModule } from './logout-routing.module';


@NgModule({
    imports: [
        NativeScriptCommonModule,
        LogoutRoutingModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        LogoutComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class LogoutModule { }
