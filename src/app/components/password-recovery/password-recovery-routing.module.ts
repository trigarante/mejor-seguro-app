import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { PasswordRecoveryComponent } from './components/password-recovery.component';

const routes: Routes = [
    {
        path: '',
        component: PasswordRecoveryComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PasswordRecoveryRoutingModule { }
